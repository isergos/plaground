import * as THREE from 'three';

export class Player {
    playerObject: THREE.Mesh<THREE.SphereGeometry>;
    playerScale = 1;
    startPlayerVelocity = 0.05;
    newPlayerVelocity = 0.05;
    newPlayerDirection = new THREE.Vector3(0, 0, 0);

    constructor() {
        const geometry = new THREE.SphereGeometry(0.5, 10, 10);
        const material = new THREE.MeshPhongMaterial({ color: 0x00ff00 });
        this.playerObject = new THREE.Mesh(geometry, material);
        this.playerObject.position.set(0, 0, 0);
        this.playerObject.name = 'sphere1';
    }
}
