import * as THREE from 'three';

import { QuadTree } from './quad-tree';
import { Player } from './player';
import { ControlManager } from './control-manager';

export class Game {
    renderer: THREE.WebGLRenderer;
    scene: THREE.Scene;
    camera: THREE.PerspectiveCamera;
    spheres = new QuadTree(-25, 25, -25, 25, 2);
    controlManager: ControlManager;

    player: Player;

    cameraToPlayerDist: number;


    constructor() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

        this.renderer = new THREE.WebGLRenderer();
        const { renderer, scene, camera } = this;
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);
        this.player = new Player();
        this.controlManager = new ControlManager(this.player, camera);

        scene.add(this.player.playerObject);
        const grid = new THREE.GridHelper(50, 50);
        grid.rotateX(Math.PI / 2);
        scene.add(grid);

        const light = new THREE.PointLight();
        light.position.set(0, 2, 60);
        scene.add(light);
        scene.add(new THREE.AmbientLight(0xffffff, 0.2));

        this.cameraToPlayerDist = 8 - this.player.playerObject.position.z;
        camera.position.z = this.cameraToPlayerDist;

        window.addEventListener('resize', () => {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize(window.innerWidth, window.innerHeight);
        });


        this.addNewSpheres();
        this.checkSphereIntersection();
        this.animate();
    }

    addNewSpheres(): void {
        const geometry = new THREE.SphereGeometry(0.3, 10, 10);
        const material = new THREE.MeshPhongMaterial({ color: 0xff0000 });

        for (let i = 0; i < 1000; i++) {
            const sphere = new THREE.Mesh(geometry, material);
            let newX: number = 0;
            let newY: number = 0;
            do {
                newX = this.randomIntFromInterval(-25, 25);
                newY = this.randomIntFromInterval(-25, 25);
            }
            while (newX > 25 || newX < -25 || newY > 25 || newY < -25);
            sphere.position.set(newX, newY, 0);
            sphere.name = `sph${i}`;
            this.spheres.appendElement(sphere);
            this.scene.add(sphere);
        }
    }

    changeSpehereRadius(): void {
        const newPlayerSphereVolume = Math.pow(this.player.playerObject.geometry.parameters.radius * this.player.playerScale, 3) * Math.PI * 4 / 3 + 0.1131;
        const newPlayerRadius = Math.cbrt((3 * newPlayerSphereVolume) / (4 * Math.PI));
        const newPlayerScale = newPlayerRadius / this.player.playerObject.geometry.parameters.radius;
        this.player.playerScale = newPlayerScale;
        this.player.playerObject.scale.setScalar(newPlayerScale);
        this.player.newPlayerVelocity = this.player.startPlayerVelocity / newPlayerScale;

        //changing camera 'z' position
        this.cameraToPlayerDist = newPlayerRadius * 2 + 7.5;
        this.camera.position.z = this.cameraToPlayerDist;
    }


    randomIntFromInterval(min: number, max: number): number {
        return Math.random() * (max - min + 1) + min;
    }

    checkSphereIntersection(): void {
        const result = new Array<THREE.Mesh<THREE.SphereGeometry>>();
        this.spheres.findSpheresAndDelete(this.player.playerObject, result);

        result.forEach((element) => {
            this.scene.remove(element);
            this.changeSpehereRadius();
        });
    }

    animate = (): void => {
        this.checkSphereIntersection();
        // if (this.player !== undefined) {
        //     if (this.leftArrowPressed) {
        //         this.player.position.x -= 0.05;
        //         this.camera.position.x -= 0.05;
        //     }
        //     if (this.rightArrowPressed) {
        //         this.player.position.x += 0.05;
        //         this.camera.position.x += 0.05;
        //     }
        //     if (this.upArrowPressed) {
        //         this.player.position.y += 0.05;
        //         this.camera.position.y += 0.05;
        //     }
        //     if (this.downArrowPressed) {
        //         this.player.position.y -= 0.05;
        //         this.camera.position.y -= 0.05;
        //     }
        // }
        if ((this.player.playerObject.position.x + this.player.newPlayerDirection.x) > 25 || (this.player.playerObject.position.x + this.player.newPlayerDirection.x) < -25) {
            this.player.newPlayerDirection.x = 0;
        }
        if ((this.player.playerObject.position.y + this.player.newPlayerDirection.y) > 25 || (this.player.playerObject.position.y + this.player.newPlayerDirection.y) < -25) {
            this.player.newPlayerDirection.y = 0;
        }
        this.player.playerObject.position.add(this.player.newPlayerDirection);
        this.camera.position.add(this.player.newPlayerDirection);
        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(this.animate);
    };
}
