import * as THREE from 'three';
import { Player } from './player';

export class ControlManager {
    player: Player;
    camera: THREE.PerspectiveCamera;
    leftArrowPressed = false;
    rightArrowPressed = false;
    upArrowPressed = false;
    downArrowPressed = false;

    constructor(player: Player, camera: THREE.PerspectiveCamera) {
        this.player = player;
        this.camera = camera;

        this._addListeners();
    }

    _addListeners(): void {
        document.body.addEventListener('mousemove', e => {
            this.player.newPlayerDirection = new THREE.Vector3(e.x - window.innerWidth / 2, -(e.y - window.innerHeight / 2), 0);
            this.player.newPlayerDirection = this.player.newPlayerDirection.normalize().multiplyScalar(this.player.newPlayerVelocity);
        });

        document.body.addEventListener('keydown', e => {
            switch (e.code) {
                case 'ArrowLeft':
                    this.leftArrowPressed = true;
                    break;
                case 'ArrowRight':
                    this.rightArrowPressed = true;
                    break;
                case 'ArrowUp':
                    this.upArrowPressed = true;
                    break;
                case 'ArrowDown':
                    this.downArrowPressed = true;
                    break;
                default:
                    break;
            }
        });

        document.body.addEventListener('keyup', e => {
            switch (e.code) {
                case 'ArrowLeft':
                    this.leftArrowPressed = false;
                    break;
                case 'ArrowRight':
                    this.rightArrowPressed = false;
                    break;
                case 'ArrowUp':
                    this.upArrowPressed = false;
                    break;
                case 'ArrowDown':
                    this.downArrowPressed = false;
                    break;
                default:
                    break;
            }
        });
    }
}
