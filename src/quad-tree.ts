import * as THREE from 'three';

class QuadTreeNode {
    minX: number;
    maxX: number;
    minY: number;
    maxY: number;
    centerX: number;
    centerY: number;
    fieldRadius: number;
    leftUpNode: QuadTreeNode | null;
    rightUpNode: QuadTreeNode | null;
    leftDownNode: QuadTreeNode | null;
    rightDownNode: QuadTreeNode | null;
    isLastLevel: boolean;
    elements: Set<THREE.Mesh<THREE.SphereGeometry>>;

    constructor(minX: number, maxX: number, minY: number, maxY: number, levels: number) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.centerX = minX + (maxX - minX) / 2;
        this.centerY = minY + (maxY - minY) / 2;
        this.fieldRadius = Math.sqrt(Math.pow((maxX - minX), 2) + Math.pow((maxY - minY), 2)) / 2;
        this.elements = new Set();

        if (levels === 0) {
            this.leftUpNode = null;
            this.rightUpNode = null;
            this.leftDownNode = null;
            this.rightDownNode = null;
            this.isLastLevel = true;
        } else {
            this.leftUpNode = new QuadTreeNode(minX, minX + (maxX - minX) / 2, maxY - (maxY - minY) / 2, maxY, levels - 1);
            this.rightUpNode = new QuadTreeNode(minX + (maxX - minX) / 2, maxX, maxY - (maxY - minY) / 2, maxY, levels - 1);
            this.leftDownNode = new QuadTreeNode(minX, minX + (maxX - minX) / 2, minY, maxY - (maxY - minY) / 2, levels - 1);
            this.rightDownNode = new QuadTreeNode(minX + (maxX - minX) / 2, maxX, minY, maxY - (maxY - minY) / 2, levels - 1);
            this.isLastLevel = false;
        }
    }

    // isContain(element: THREE.Mesh<THREE.SphereGeometry>): boolean {
    //     console.log('isContain fuction');
    //     if (element.position.x >= this.minX
    //         && element.position.x < this.maxX
    //         && element.position.y >= this.minY
    //         && element.position.y < this.maxY) {
    //         return true;
    //     }
    //     return false;
    // }

    isIntersect(element: THREE.Mesh<THREE.SphereGeometry>): boolean {
        const distX = Math.abs(element.position.x - this.centerX);
        const distY = Math.abs(element.position.y - this.centerY);
        const halfRectW = Math.abs((this.maxX - this.minX) / 2);
        const halfRectH = Math.abs((this.maxY - this.minY) / 2);

        if (distX > (element.geometry.parameters.radius + halfRectW)) { return false; }
        if (distY > (element.geometry.parameters.radius + halfRectH)) { return false; }

        if (distX <= halfRectW) { return true; }
        if (distY <= halfRectH) { return true; }

        const dx = distX - halfRectW;
        const dy = distY - halfRectH;

        return (dx * dx + dy * dy <= (element.geometry.parameters.radius * element.geometry.parameters.radius));
    }

    append(element: THREE.Mesh<THREE.SphereGeometry>): void {
        let intersectionCounter = 0;
        let lastInersectedPart;
        if (this.isLastLevel) {
            this.elements?.add(element);
        } else {
            if (this.leftUpNode?.isIntersect(element)) {
                lastInersectedPart = this.leftUpNode;
                intersectionCounter++;
            }
            if (this.rightUpNode?.isIntersect(element)) {
                lastInersectedPart = this.rightUpNode;
                intersectionCounter++;
            }
            if (this.leftDownNode?.isIntersect(element)) {
                lastInersectedPart = this.leftDownNode;
                intersectionCounter++;
            }
            if (this.rightDownNode?.isIntersect(element)) {
                lastInersectedPart = this.rightDownNode;
                intersectionCounter++;
            }

            if (intersectionCounter > 1) {
                this.elements?.add(element);
            } else {
                lastInersectedPart?.append(element);
            }
        }
    }

    // _isElementItersectField(player: THREE.Mesh<THREE.SphereGeometry>): boolean {
    //     const dx = player.position.x - this.centerX;
    //     const dy = player.position.y - this.centerY;
    //     const distance = Math.sqrt(dx * dx + dy * dy);
    //     const sumRad = player.geometry.parameters.radius * player.scale.x + this.fieldRadius;

    //     if (distance < sumRad) {
    //         return true;
    //     }
    //     return false;
    // }
    checkSphereIntersection(player: THREE.Mesh<THREE.SphereGeometry>, result: Array<THREE.Mesh<THREE.SphereGeometry>>): void {
        for (const elem of this.elements) {
            const dx = player.position.x - elem.position.x;
            const dy = player.position.y - elem.position.y;
            const distance = Math.sqrt(dx * dx + dy * dy);
            const sumRad = player.geometry.parameters.radius * player.scale.x + elem.geometry.parameters.radius;

            if (distance < sumRad) {
                result.push(elem);
                this.elements.delete(elem);
            }
        }
    }

    findElementsAndDelete(player: THREE.Mesh<THREE.SphereGeometry>, result: Array<THREE.Mesh<THREE.SphereGeometry>>): void {
        let intersectionCounter = 0;
        if (this.isLastLevel) {
            this.checkSphereIntersection(player, result);
        } else {
            if (this.leftUpNode?.isIntersect(player)) {
                this.leftUpNode.findElementsAndDelete(player, result);
                intersectionCounter++;
            }
            if (this.rightUpNode?.isIntersect(player)) {
                this.rightUpNode.findElementsAndDelete(player, result);
                intersectionCounter++;
            }
            if (this.leftDownNode?.isIntersect(player)) {
                this.leftDownNode.findElementsAndDelete(player, result);
                intersectionCounter++;
            }
            if (this.rightDownNode?.isIntersect(player)) {
                this.rightDownNode.findElementsAndDelete(player, result);
                intersectionCounter++;
            }

            if (intersectionCounter > 1) {
                this.checkSphereIntersection(player, result);
            }
        }
    }
}

export class QuadTree {
    private _root: QuadTreeNode | null;
    readonly count = 0;

    constructor(minX: number, maxX: number, minY: number, maxY: number, levels: number) {
        this._root = new QuadTreeNode(minX, maxX, minY, maxY, levels);
    }

    appendElement(element: THREE.Mesh<THREE.SphereGeometry>): void {
        if (this._root) {
            this._root.append(element);
        }
    }

    findSpheresAndDelete(player: THREE.Mesh<THREE.SphereGeometry>, result: Array<THREE.Mesh<THREE.SphereGeometry>>): void {
        if (this._root) {
            this._root.findElementsAndDelete(player, result);
        }
    }
}
