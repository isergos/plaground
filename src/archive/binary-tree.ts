// Binary tree //////////////////////////

class TreeNode {
    value: number;
    leftNode: TreeNode | null;
    rightNode: TreeNode | null;

    constructor(value: number) {
        this.value = value;
        this.leftNode = null;
        this.rightNode = null;
    }
}
type NoReadonly<T> = { -readonly [P in keyof T]: T[P] };

export class BinaryTree {
    private _root: TreeNode | null;
    readonly count = 0;

    constructor() {
        this._root = null;
    }

    add(value: number): void {
        if (this._root == null) {
            const rootNode = new TreeNode(value);
            this._root = rootNode;
        } else {
            this.addTo(this._root, value);
        }
        (this as NoReadonly<BinaryTree>).count++;
    }

    addTo(node: TreeNode, value: number): void {
        if (value < node.value) {
            if (node.leftNode == null) {
                node.leftNode = new TreeNode(value);
            } else {
                this.addTo(node.leftNode, value);
            }
        } else {
            if (node.rightNode == null) {
                node.rightNode = new TreeNode(value);
            } else {
                this.addTo(node.rightNode, value);
            }
        }
    }

    remove(value: number): void | null {
        let currentNode = this._root;
        let parentNode;

        if (currentNode === null) {
            return null;
        }

        while (currentNode !== null) {
            console.log(currentNode.value);
            if (value < currentNode.value) {
                parentNode = currentNode;
                currentNode = currentNode.leftNode;
            } else if (value > currentNode.value) {
                parentNode = currentNode;
                currentNode = currentNode.rightNode;
            } else {
                break;
            }
        }
        console.log('removed value is ', currentNode?.value);
        if (currentNode?.rightNode == null) {
            if (currentNode?.leftNode && currentNode.leftNode.value < parentNode?.value!) {
                if (parentNode?.leftNode) {
                    console.log(parentNode.value);
                    parentNode.leftNode = currentNode.leftNode;
                }
            } else {
                if (parentNode?.rightNode) {
                    parentNode.rightNode = currentNode;
                }
            }
        } else if (currentNode.rightNode && currentNode.rightNode?.leftNode === null && currentNode.rightNode.rightNode) {
            const resultNode = currentNode.rightNode;
            resultNode.leftNode = currentNode.leftNode

            if (currentNode.value < parentNode?.value!) {
                if (parentNode?.leftNode) {
                    parentNode.leftNode = resultNode;
                }
            } else {
                if (parentNode?.rightNode) {
                    parentNode.rightNode = resultNode;
                }
            }
        }
    }

    search(value: number) {
        let currentNode = this._root;

        if (currentNode == null) {
            return null;
        }

        while (currentNode != null) {
            console.log(currentNode.value);
            if (value < currentNode.value) {
                currentNode = currentNode.leftNode;
            } else if (value > currentNode.value){
                currentNode = currentNode.rightNode;
            } else {
                break;
            }
        }

        return currentNode;
    }
}