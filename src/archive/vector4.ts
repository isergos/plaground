import { Matrix4 } from './matrix4';

export class Vector4 extends Float32Array {
    constructor(x: number, y: number, z: number, w: number) {
        super(4);
        this[0] = x;
        this[1] = y;
        this[2] = z;
        this[3] = w;
    }

    add(vec: Vector4): void {
        this[0] += vec[0];
        this[1] += vec[1];
        this[2] += vec[2];
    }

    substract(vec: Vector4): void {
        this[0] -= vec[0];
        this[1] -= vec[1];
        this[2] -= vec[2];
    }

    multiplyByNumber(num: number): void {
        this[0] *= num;
        this[1] *= num;
        this[2] *= num;
    }

    dotMultiply(vec: Vector4): number {
        return this[0] * vec[0] + this[1] * vec[1] + this[2] * vec[2];
    }

    vecMultiply(vec: Vector4): Vector4 {
        return new Vector4(
            this[1] * vec[2] - this[2] * vec[1],
            this[2] * vec[0] - this[0] * vec[2],
            this[0] * vec[1] - this[1] * vec[0],
            1
        );
    }

    devideByNumber(num: number): void {
        this[0] /= num;
        this[1] /= num;
        this[2] /= num;
    }

    private _multiplyOnMatrix(mat: Matrix4): void {
        const tempVec = this;
        this[0] = mat.arr[0] * tempVec[0] + mat.arr[1] * tempVec[1] + mat.arr[2] * tempVec[2] + mat.arr[3] * tempVec[3];
        this[1] = mat.arr[4] * tempVec[0] + mat.arr[5] * tempVec[1] + mat.arr[6] * tempVec[2] + mat.arr[7] * tempVec[3];
        this[2] = mat.arr[8] * tempVec[0] + mat.arr[9] * tempVec[1] + mat.arr[10] * tempVec[2] + mat.arr[11] * tempVec[3];
        this[3] = mat.arr[12] * tempVec[0] + mat.arr[13] * tempVec[1] + mat.arr[14] * tempVec[2] + mat.arr[15] * tempVec[3];
    }

    translate(x: number, y: number, z: number): void {
        const translateMatrix = new Matrix4().translate(x, y, z);
        this._multiplyOnMatrix(translateMatrix);
    }

    scale(x: number, y: number, z: number): void {
        const translateMatrix = new Matrix4().scale(x, y, z);
        this._multiplyOnMatrix(translateMatrix);
    }
}
