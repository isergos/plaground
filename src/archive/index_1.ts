import * as THREE from 'three';
import { QuadTree } from '../quad-tree';

class NodeObj {
    name: string;
    x: number;
    y:number;

    constructor(name: string, x: number, y: number) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
}

class Node {
    elements: Set<NodeObj>;
    constructor() {
        this.elements = new Set();
    }

    add(elem: NodeObj): void {
        this.elements.add(elem);
    }

    findIntersection(result: Array<NodeObj>): void {
        for (const obj of this.elements) {
            if (obj.name === 'object3' || obj.name === 'object5' || obj.name === 'object7') {
                result.push(obj);
                this.elements.delete(obj);
            }
        }
    }
}

export class Game {
    constructor() {
        const gameField = new QuadTree(-25, 25, -25, 25, 2);

        const geometryP = new THREE.SphereGeometry(5, 10, 10);
        const materialP = new THREE.MeshPhongMaterial({ color: 0x00ff00 });
        const player = new THREE.Mesh(geometryP, materialP);
        player.position.set(0.6, 0, 0);

        const geometry = new THREE.SphereGeometry(0.3, 10, 10);
        const material = new THREE.MeshPhongMaterial({ color: 0xff0000 });

        const sphere = new THREE.Mesh(geometry, material);
        sphere.position.set(0, 0, 0);
        const sphere2 = new THREE.Mesh(geometry, material);
        sphere2.position.set(20, 5, 0);
        const sphere3 = new THREE.Mesh(geometry, material);
        sphere3.position.set(5, 5, 0);

        gameField.appendElement(sphere);
        gameField.appendElement(sphere2);
        gameField.appendElement(sphere3);
    }

    randomIntFromInterval(min: number, max: number): number {
        return Math.random() * (max - min + 1) + min;
    }
}
