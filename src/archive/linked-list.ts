// Linked List ///////////////////////////////
class ListNode<T> {
    element: T;
    nextNode: ListNode<T> | null;
    constructor(element: T) {
        this.element = element;
        this.nextNode = null;
    }

}

export class LinkedList<T> {
    private head: ListNode<T> | null;
    private length = 0;

    constructor() {
        this.head = null;
    }

    append(element: T) {
        let node = new ListNode<T>(element);

        if (this.head === null) {
            this.head = node;
        } else {
            let tempNode: ListNode<T> = this.head;
            while(tempNode.nextNode) {
                tempNode = tempNode.nextNode!;
            }
            tempNode.nextNode = node;

        }

        this.length++;
    }

    insert(element: T, position: Number) {
        if (position < this.length) {
            let node = new ListNode<T>(element);
            let tempNode = this.head;
            let prevNode;
            let index = 0;

            if (position === 0) {
                node.nextNode = this.head;
                this.head = node;
            } else {
                while (index < position) {
                    if (tempNode?.nextNode) {
                        prevNode = tempNode;
                        tempNode = tempNode.nextNode;
                    }
                    index++;
                }
                node.nextNode = tempNode;
                if (prevNode)
                prevNode.nextNode = node;
            }

            this.length++;
        } else {
            return null;
        }
    }

    searchByValue(value: T) {
        let position = 1;
        let tempNode = this.head;

        if (tempNode && tempNode.element === value) {
            return 0;
        }

        while(tempNode && tempNode?.nextNode !== null) {
            if (tempNode.nextNode?.element === value) {
                return position;
            } else {
                tempNode = tempNode.nextNode;
                position++;
            }
        }

        return null;
    }

    searchByPosition(position: number) {
        let index = 0;
        let tempNode = this.head;

        if(position >= 0 && position < this.length) {
            while(index !== position) {
                if (tempNode?.nextNode) {
                    tempNode = tempNode.nextNode;
                }
                index++;
            }

            return tempNode?.element;
        } else {
            return null;
        }
    }

    removeAt(pos: Number) {
        if (pos > -1 && pos < this.length) {
            let tempNode = this.head;
            let prevNode;
            let index = 0;

            if (pos === 0) {
                if (tempNode?.nextNode) {
                    this.head = tempNode.nextNode;
                }
            } else {
                while(index < pos) {
                    if(tempNode?.nextNode) {
                        prevNode = tempNode;
                        tempNode = tempNode.nextNode;
                    }
                    index++;
                }
                if (prevNode?.nextNode && tempNode) {
                    prevNode.nextNode = tempNode?.nextNode;
                }
                

            }
            this.length--
        } else {
            return null;
        }
    }
}