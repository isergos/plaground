export class Matrix4 {
    public arr: Float32Array;
    constructor() {
        this.arr = new Float32Array(16);
    }

    identity(): this  {
        for (let i = 0; i < 16; i++) {
            this.arr[0] = 0;
        }
        this.arr[0] = 1;
        this.arr[5] = 1;
        this.arr[10] = 1;
        this.arr[15] = 1;

        return this;
    }

    translate(x: number, y: number, z: number): this {
        this.identity();

        this.arr[3] = x;
        this.arr[7] = y;
        this.arr[11] = z;

        return this;
    }

    scale(x: number, y: number, z: number): this {
        this.identity();

        this.arr[0] = x;
        this.arr[5] = y;
        this.arr[10] = z;

        return this;
    }
}
